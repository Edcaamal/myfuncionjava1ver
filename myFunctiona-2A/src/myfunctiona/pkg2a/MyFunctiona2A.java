/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myfunctiona.pkg2a;


public class MyFunctiona2A {
    
    static void encabezado(){
        System.out.println("                   Primer proyecto con GitLab");
        System.out.println("          Aplicación para demostrar las problematicas");
        System.out.println("              para los estyudiantes de la UAC FI ISC");
        System.out.println("Derivados de una Pandemia Mundial por el CORONAVIRUS COVID-19");
        separador(true);
    }
    static void cuepoPrograma(int numeracion, String cadena){
        if (numeracion == 1 ){
            System.out.println("1.-" );
            System.out.println("2.-");
            System.out.println("3.-");
            System.out.println("4.-");
            if (cadena == "Yesica"){
               System.out.println("Bienvenida Yesica a la mejor Universidad" );
                
            }
        }
        if (numeracion == 2 ){
            if (cadena == "Juan"){
               System.out.println("Juan pon atencion en las clases" );
            }
            System.out.println("A.-");
            System.out.println("B.-");
            System.out.println("C.-");
            System.out.println("D.-");
        }
        if (numeracion == 3 ){
            System.out.println("@.-");
            System.out.println("$.-");
            System.out.println("%.-");
            System.out.println("&.-");
        }
    }

    static void separador(boolean tipoSeparador){
        if (tipoSeparador){
           System.out.println("==================================================================");
        } else {
           System.out.println("------------------------------------------------------------------");
        }
    }
    static void piePagina(){
        separador(true);
        System.out.println("(c) UAC FI ISC LP1");
    }


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        encabezado();
        cuepoPrograma(2, "Juan");
        separador(false);
        cuepoPrograma(1, "Yesica");
        separador(false);
        cuepoPrograma(3, "Luis");
        piePagina();
                
    }
    
}
